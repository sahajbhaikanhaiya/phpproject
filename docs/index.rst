.. Php Crud App. documentation master file, created by
   sphinx-quickstart on Thu Mar 26 22:05:23 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Php Crud App.'s documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


Another Simple header
=====================

.. code-block:: php
   :emphasize-lines: 3,5

   def some_function():
       interesting = False
       print 'This line is highlighted.'
       print 'This one is not...'
       print '...but this one is.'



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
